//
// Created by 张思源 on 2022/4/18.
//

#include "bank.h"
void getTime(int N,int a[],int b[],int K);
int getMin(int a[],int n);
int getMax(int wait[],int n);

double avg(LinkQueue Q[],int K,int N);
int getMindoor(LinkQueue Q[],int K,int Into);
int getWaitTime(LinkQueue Q,int Into);
int getNowTime(LinkQueue Q,int Into,int time);


int main(){
    int N,K;
    scanf("%d",&N);
    int a[N];//到达时间
    int b[N];//处理时间
    for(int i=0;i<N;i++){
        scanf("%d %d",&a[i],&b[i]);
        if(b[i]>60){
            b[i]=60;
        }
    }
    scanf("%d",&K);
    getTime(N,a,b,K);
    return 0;
}

void getTime(int N,int a[],int b[],int K){
    LinkQueue Q[K];//创建K个窗口,一个窗口一个队列
    for(int i=0;i<K;i++){
        LinkQueue q;
        InitQueue(q);
        Q[i]=q;
    }
    int i;//遍历
    int watm;//等待时间
    int nowt;//当前时间
    int Into;//到达时间
    int Time;//处理时间
    //遍历顾客,i为当前顾客
    for(i=0;i<N;i++){
        Into=a[i];
        Time=b[i];
        int minDoor= getMindoor(Q,K,Into);//窗口号
        watm= getWaitTime(Q[minDoor],Into);

        nowt= getNowTime(Q[minDoor],Into,Time);
//        printf("%d\n",watm);

        EnQueue(Q[minDoor],Time,Into,watm,nowt);
    }
    int Allwitetime[N];
    int lastTime[K];
    int x=0;
    double avg;
    double sum=0;
    for(int i=0;i<K;i++){
        lastTime[i]=Q[i].rear->nowtime;
        QueuePtr p=Q[i].front->next;
        while (p){
            Allwitetime[x]=p->wait;
            x++;
            p=p->next;
        }
    }
    for(x=0;x<N;x++){
        sum=sum+Allwitetime[x];
    }
    avg=sum/N;
    printf("%.1lf ",avg);
    printf("%d ", getMax(Allwitetime,N));
    printf("%d\n", getMax(lastTime,K));
    for(int i=0;i<K;i++){
        printf("%d ", QueueLength(Q[i]));
    }
}



int getMin(int a[],int n){
    int min=a[0];
    for(int i=0;i<n;i++){
        if(min>a[i])
            min=a[i];
    }

    return min;
}
int getMax(int wait[],int n){
    int max=wait[0];
    for(int i=0;i<n;i++){
        if(max<wait[i]){
            max=wait[i];
        }
    }
    return max;
}
int getMindoor(LinkQueue Q[],int K,int Into){
    int stime[K];
    for(int i=0;i<K;i++){
        if(Q[i].front==Q[i].rear){
            return i;
        }
        if(Into>Q[i].rear->nowtime){
            return i;
        }
        stime[i]=Q[i].rear->nowtime-Into;
    }
    int min= getMin(stime,K);
    for(int i=0;i<K;i++){
        if(stime[i]==min){
            return i;
        }
    }
}
int getWaitTime(LinkQueue Q,int Into){
    if(Q.rear==Q.front){
        return 0;
    } else if(Q.rear->nowtime<Into){
        return 0;
    }
    else
        return Q.rear->nowtime-Into;
}
int getNowTime(LinkQueue Q,int Into,int time){
    if(Q.rear==Q.front){
        return  Into+time;
    }
    else if(Into>=Q.rear->nowtime)
        return time+Into;
    else
        return Q.rear->nowtime+time;
}
