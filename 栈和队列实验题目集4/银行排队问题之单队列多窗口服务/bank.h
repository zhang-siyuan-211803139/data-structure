//
// Created by 张思源 on 2022/4/16.
//

#include <stdio.h>
#include <malloc.h>

#define OK 1
#define ERROR 0
typedef int Status;
typedef int QElemType;  //队列中的元素类型

typedef struct QNode  //队列中的节点类型
{
    QElemType time;//事务处理时间
    struct QNode *next;
    int into;//到达时间
    int  wait;//等待时间
    int nowtime;//当前的时间
}QNode,*QueuePtr;

typedef struct
{
    QueuePtr front;
    QueuePtr rear;
}LinkQueue;   //定义连队列类型


Status InitQueue(LinkQueue & Q);
//初始化一个空队列，即分配头节点。头指针和尾指针初始化在头节点位置。成功返回OK，失败返回ERROR。
Status GetHead(LinkQueue Q,QElemType &e);
//获取队头元素的值并返回在e中，取值成功返回OK，失败返回ERROR
Status EnQueue(LinkQueue &Q,QElemType time,int into,int wait,int nowtime);
//将元素e入队成为新的队尾元素
Status DeQueue(LinkQueue &Q,QElemType &x);
//将队头元素出队 ，其值返回在e中。出队成功返回OK，失败返回ERROR。
int QueueLength(LinkQueue Q);
//求队列长度操作，即返回实际元素个数。

Status InitQueue(LinkQueue & Q){
    Q.front=Q.rear=(QueuePtr)malloc(sizeof(QNode));
    if(!Q.front) return ERROR;
    Q.front->next=NULL;
    return OK;
}

//获取队头元素的值并返回在e中，取值成功返回OK，失败返回ERROR
Status EnQueue(LinkQueue &Q,QElemType time,int into,int wait,int nowtime){
    QueuePtr p=(QueuePtr)malloc(sizeof(QNode));
    if(!p) return ERROR;
    p->time=time;
    p->into=into;
    p->wait=wait;
    p->nowtime=nowtime;
    p->next=NULL;
    Q.rear->next=p;
    Q.rear=p;
    return OK;
}
//将元素e入队成为新的队尾元素
Status DeQueue(LinkQueue &Q,QElemType &x){
    if(Q.front==Q.rear) return ERROR;
    QueuePtr  p=Q.front->next;
//    x=p->data;
    Q.front->next=p->next;
    if(Q.rear ==p) Q.rear=Q.front;
    free(p);
    return OK;

}
//将队头元素出队 ，其值返回在e中。出队成功返回OK，失败返回ERROR。
int QueueLength(LinkQueue Q){
    int i=0;
    QueuePtr p=Q.front;
    while(p!=Q.rear){
        p=p->next;
        i++;
    }
    return i;
}
//求队列长度操作，即返回实际元素个数。


