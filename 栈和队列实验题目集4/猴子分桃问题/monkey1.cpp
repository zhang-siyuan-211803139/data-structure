//
// Created by 张思源 on 2022/4/16.
//

#include "monkey1.h"
void  getPeach(int n,int k,int m);

int main(){
    int n,k,m;
    scanf("%d %d %d",&n,&k,&m);
    getPeach(n,k,m);
}


void  getPeach(int n,int k,int m){
    LinkQueue M;
    InitQueue(M);
    //初始化队列
    for(int j=1;j<=n;j++){
        EnQueue(M,0,j);
    }
    int kuang=1;//框中桃子数，初始为1
    int buchong=1;//补充数
    while(QueueLength(M)){
        int e,x;
        GetHead(M,e,x);
        if(e+kuang<m){
            DeQueue(M,e,x);
            e=e+kuang;
            EnQueue(M,e,x);
//            printf("%d ",e);
            buchong++;
            if(buchong>k){
                buchong=1;//框中满K，回滚1
            }
            kuang=buchong;
        } else if(e+kuang==m){
            DeQueue(M,e,x);
            printf("%d ",x);
            kuang=0;
            buchong++;
            if(buchong>k){
                buchong=1;//框中满K，回滚1
            }
            kuang=buchong;
        }
        else{
            DeQueue(M,e,x);
            kuang=kuang-(m-e);
            printf("%d ",x);
        }
    }

}