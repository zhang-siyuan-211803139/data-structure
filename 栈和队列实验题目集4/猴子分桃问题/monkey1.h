//
// Created by 张思源 on 2022/4/16.
//


#define UNTITLED1_MONKEY1_H

//
// Created by 张思源 on 2022/4/16.
//


#define UNTITLED1_MONKEY_H

#include <stdio.h>
#include <malloc.h>

#define OK 1
#define ERROR 0
typedef int Status;
typedef int QElemType;  //队列中的元素类型;
typedef struct QNode  //队列中的节点类型
{
    QElemType data; //桃子数
    struct QNode *next;
    int  nums;
}QNode,*QueuePtr;

typedef struct
{
    QueuePtr front;
    QueuePtr rear;
}LinkQueue;   //定义连队列类型
Status InitQueue(LinkQueue & Q);
//初始化一个空队列，即分配头节点。头指针和尾指针初始化在头节点位置。成功返回OK，失败返回ERROR。
Status GetHead(LinkQueue Q,QElemType &e,int &x);
//获取队头元素的值并返回在e中，取值成功返回OK，失败返回ERROR
Status EnQueue(LinkQueue &Q,QElemType e,int nums);
//将元素e入队成为新的队尾元素
Status DeQueue(LinkQueue &Q,QElemType &x,int &nums);
//将队头元素出队 ，其值返回在e中。出队成功返回OK，失败返回ERROR。
int QueueLength(LinkQueue Q);
//求队列长度操作，即返回实际元素个数。
Status InitQueue(LinkQueue & Q){
    Q.front=Q.rear=(QueuePtr)malloc(sizeof(QNode));
    if(!Q.front) return ERROR;
    Q.front->next=NULL;
    return OK;
}
//初始化一个空队列，即分配头节点。头指针和尾指针初始化在头节点位置。成功返回OK，失败返回ERROR。
Status GetHead(LinkQueue Q,QElemType &e,int &x){
    if(Q.front==Q.rear)return ERROR;
    x=Q.front->next->nums;
    e=Q.front->next->data;
    return OK;
}
//获取队头元素的值并返回在e中，取值成功返回OK，失败返回ERROR
Status EnQueue(LinkQueue &Q,QElemType e,int nums){
    QueuePtr p=(QueuePtr)malloc(sizeof(QNode));
    if(!p) return ERROR;
    p->nums=nums;
    p->data=e;
    p->next=NULL;
    Q.rear->next=p;
    Q.rear=p;
    return OK;
}
//将元素e入队成为新的队尾元素
Status DeQueue(LinkQueue &Q,QElemType &x,int &nums){

    if(Q.front==Q.rear) return ERROR;
    QueuePtr  p=Q.front->next;
    x=p->data;
    nums=p->nums;
    Q.front->next=p->next;
    if(Q.rear ==p) Q.rear=Q.front;
    free(p);
    return OK;

}
//将队头元素出队 ，其值返回在e中。出队成功返回OK，失败返回ERROR。
int QueueLength(LinkQueue Q){
    int i=0;
    QueuePtr p=Q.front;
    while(p!=Q.rear){
        p=p->next;
        i++;
    }
    return i;
}
//求队列长度操作，即返回实际元素个数。

