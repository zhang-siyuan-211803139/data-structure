#include <iostream>
#include <stdio.h>
#include <malloc.h>

#define STACK_INIT_SIZE 50
#define STACKINCREMENT 10
#define OK 1
#define ERROR 0
#define MaxSize 100
#define N 8

typedef struct {
    int m[N+2][N+2];
    int footprint[N+2][N+2]; //足迹，到达过为1 默认为0；
}MazeType;// 定义地图
MazeType M;
typedef int Status;
typedef struct {
    int x;
    int y;
}PosType;
typedef struct{
    int ord;   //序号
    PosType seat;
    int di;//方向
}SElemType;           //定义元素
typedef struct
{   SElemType *base;
    SElemType *top;
    int stacksize;			//栈顶指针
} SqStack;              //顺序栈类型


Status InitStack(SqStack &S);     //构造一个空栈S，创建成功返回OK，否则返回ERROR
Status Push(SqStack &S,SElemType e);  //进栈操作，将元素e进栈，为栈顶元素
Status Pop(SqStack &S,SElemType &e); //出栈操作，若栈不为空，删除S的栈顶元素，用e返回其值，成功返回OK，否则返回ERROR
Status GetTop(SqStack S,SElemType &e);  //获取栈顶元素an,返回在e中,取值失败返回ERROR，成功返回OK
int StackLength(SqStack S);  //返回栈中元素个数
Status StackEmpty(SqStack S); //判断是否为空栈
Status Pass(PosType a);//判断该位置能不能通过
PosType Nextpos(PosType a,int di);//方向移动到下一个点
void  FootPrint(PosType a);//到达过标记
Status MazePath(MazeType maze,PosType start,PosType end);
void PrintfStack( SqStack S);//打印路径
void MarkPrint(PosType seat);//标记为不能通过
SqStack S;
int main() {

    int x;
    scanf("%d",&x);
    //输入数组
    for(int i=0;i<x;i++){
        for(int j=0;j<x;j++){
            scanf("%d",&M.m[i][j]);
            //初始化足迹为0
            M.footprint[i][j]=0;
        }
    }

    PosType start={1,1};

    PosType end={x-2,x-2};

    MazePath(M,start,end);

    PrintfStack(S);


}

//构造一个空栈S，创建成功返回OK，否则返回ERROR
Status InitStack(SqStack &S){
    S.base=(SElemType *)malloc(STACK_INIT_SIZE*sizeof(SElemType));
    if(!S.base) return ERROR;
    S.top=S.base;
    S.stacksize=STACK_INIT_SIZE;
    return OK;
}
//进栈操作，将元素e进栈，为栈顶元素
Status Push(SqStack &S,SElemType e){
    if(S.top-S.base>=S.stacksize){
        SElemType *p;
        p=(SElemType *) realloc(S.base,S.stacksize+STACKINCREMENT*sizeof (SElemType));
        if(!p)return ERROR;
        S.base=p;
        S.top=S.base+S.stacksize;
        S.stacksize+=STACKINCREMENT;
    }
    *S.top++=e;
    return OK;
}
//出栈操作，若栈不为空，删除S的栈顶元素，用e返回其值，成功返回OK，否则返回ERROR
Status Pop(SqStack &S,SElemType &e){
    if(S.top==S.base) return ERROR;
    S.top--;
    e=*S.top;
    return OK;

}
//获取栈顶元素an,返回在e中,取值失败返回ERROR，成功返回OK
Status GetTop(SqStack S,SElemType &e){
    if(S.top==S.base) return  ERROR;
    SElemType * p=(--S.top);
    e=*p;
    return OK;

}
//返回栈中元素个数
int StackLength(SqStack S){
    return S.top-S.base;
}
//判断栈是否为空
Status StackEmpty(SqStack S){
    if(S.top==S.base)
        return OK;
    else
        return ERROR;
}
//判断当前位置是否走过
Status Pass(PosType a){
        if(M.m[a.x][a.y]==0 && M.footprint[a.x][a.y]==0){
        return OK;
    } else{
        return ERROR;
    }

}


//方向移动到下一个点
PosType Nextpos(PosType a,int di){
    switch (di) {
        case 1:a.y++;break;
        case 2:a.x++;break;
        case 3:a.y--;break;
        case 4:a.x--;break;
    }
    return a;

}
//到达过标记，防止重复到达
void  FootPrint(PosType a){
    M.footprint[a.x][a.y]=1;
    return;
}

//计算路线
Status MazePath(MazeType maze,PosType start,PosType end){
    //初始化一个空栈

    InitStack(S);
    PosType curpos=start;
    SElemType e;
    int curstep =1 ;//探索步数
    do{
        if(Pass(curpos)){
            FootPrint(curpos);
            e={curstep,curpos,1};
            Push(S,e);
            if(curpos.x==end.x && curpos.y==end.y) return OK;
            curpos= Nextpos(curpos,1);
            curstep++;
        } else{
            if(!StackEmpty(S)){
                Pop(S,e);
                while (e.di==4 && !StackEmpty(S))
                {
                    MarkPrint(e.seat);
                    Pop(S,e);
                }
                if(e.di<4){
                    e.di++;
                    Push(S,e);
                    curpos= Nextpos(e.seat,e.di);
                }
            }
        }
    } while (!StackEmpty(S));
    return ERROR;
}

//打印路径
void PrintfStack( SqStack S){
    SElemType *i=S.base;
    while(i!=S.top){
        printf("(%d,%d)",i->seat.x,i->seat.y);
        i++;
    }
    //没有路径即为NO
    if(S.base==S.top){
        printf("NO");
    }
    return;
}
void MarkPrint(PosType seat)
{
    M.m[seat.x][seat.y]=1;//不能通过即为墙
}
//标记为不能通过