//
// Created by 张思源 on 2022/4/15.
//

#include "LinkQueue.h"
void BankService(int n);
int main(){
    int n;
    scanf("%d",&n);
    BankService(n);
}
void BankService(int n){
    LinkQueue QA,QB;
    InitQueue(QA);
    InitQueue(QB);
    int j;
    QElemType x;
    for(j=0;j<n;j++){
        scanf("%d",&x);
        if(x%2==0){
            EnQueue(QB,x);
        } else {
            EnQueue(QA,x);
        }
    }
    QElemType e;
    while (QueueLength(QA) && QueueLength(QB) ){
        DeQueue(QA,e);
        printf("%d ",e);
        DeQueue(QA,e);
        printf("%d ",e);
        DeQueue(QB,e);
        printf("%d ",e);
    }
    while (QueueLength(QA)){
        DeQueue(QA,e);
        printf("%d ",e);
    }
    while(QueueLength(QB)){
        DeQueue(QB,e);
        printf("%d ",e);
    }

}